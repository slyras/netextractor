# Dummy builder for curlexample project.

CC          = gcc
LD          = gcc

CFLAGS      = -g -O0 -Wall -D_REENTRANT #-DNDEBUG
LDFLAGS     = -lcurl
LIB_OUTPUTFILE  = mylib
OUTPUTFILE  = curlexample
        
#Source Directories
# Header file paths
CURRENT_DIR = $(shell pwd)

LIB_INCLUDES	= ./inc/ \

# Source file paths
LIB_SOURCE_PATHS	= ./src \
        
APP_SOURCE_PATHS = .

# Gather all header files
INCLUDE_FILES	=$(foreach d, $(LIB_INCLUDES), $(wildcard $d*.h))
# Gather all header paths and add -I flag
INC_PARAMS	=$(foreach d, $(LIB_INCLUDES), -I $d)
# Gather all source files
LIB_SOURCE_FILES   	= $(foreach d, $(LIB_SOURCE_PATHS), $(wildcard $d/*.c))
                                                          
APP_SOURCE_FILES    = $(foreach d, $(APP_SOURCE_PATHS), $(wildcard $d/*.c))                                                          
                                                      
# Gather all object files (source files changing ending)
LIB_OBJECTS         = $(patsubst %.c,%.o,$(LIB_SOURCE_FILES))
                                                          
APP_OBJECTS         = $(patsubst %.c,%.o,$(APP_SOURCE_FILES))                                                          

# Rule for converting source files to object files (compile)
%.o: %.c $(INCLUDE_FILES)
	$(CC) -c $(CFLAGS) $(INC_PARAMS) -o $@ $< -fPIC

# Make all rule (linking)
lib : $(LIB_OBJECTS)
	$(LD) -shared -o lib$(LIB_OUTPUTFILE).so $(LIB_OBJECTS) $(LDFLAGS)
                                                      	
                                                      
# Make all rule (linking)
all : lib $(APP_OBJECTS)
	@echo "Linking..."
	$(LD) $(APP_OBJECTS) -o $(OUTPUTFILE) -L$(CURRENT_DIR) -l$(LIB_OUTPUTFILE)
	size $(OUTPUTFILE)

.PHONY: clean

# Make clean rule (removing binaries and object files)
clean:
	@echo "Clean...."
	rm -rf $(APP_OBJECTS)
	rm -rf $(LIB_OBJECTS)
	rm -rf lib$(LIB_OUTPUTFILE).so
	rm -rf ./$(OUTPUTFILE)

# Make info rule (just show some stuff)
info:
	@echo "Header Files....."
	@echo $(INCLUDE_FILES)
	@echo "Source Files....."
	@echo $(APP_SOURCE_FILES)
	@echo $(LIB_SOURCE_FILES)

check:
	@echo "Checking...."
	cppcheck --std=posix --enable=portability $(APP_SOURCE_FILES)
	cppcheck --std=posix --enable=portability $(LIB_SOURCE_FILES)

runcheck: all
	@echo "Run time Check...."
	valgrind ./$(OUTPUTFILE)

