#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include "mylib.h"

int main(int argc, char* argv[]){
    
    char* UserURL       = "http://www.google.com";
    myLib_req_t* newrq  = NULL;
    myLib_info_t *info  = NULL;
    int i=0,opt=0,ReqNum=1,failures=0,validData=0;
    
    double NETWORLLOOKUP_TIME   = 0.0;
    double CONNECT_TIME         = 0.0;
    double TRANSFER_TIME        = 0.0;
    double TOTAL_TIME           = 0.0;
    
    char LASTIP_ADDRESS[MAXIMUM_IP_ADDRESS] = {0};
    long LASTRESP_CODE = 0;
    
    if(myLib_init()!=MY_LIB_SUCCESS) {
        printf("Failed to initilize!");
        return EXIT_FAILURE;
    }
    
    newrq = myLib_new_req();
    if(!newrq){
        printf("Failed to initialize request");
        myLib_cleanup();
        return EXIT_FAILURE;
    }
    
    while (( opt = getopt(argc, argv, "n:H:u:")) != -1){
        switch (opt) {
            case 'u':
                UserURL = optarg;
                printf("Updating URL [%s]\n",UserURL);
                break;
            case 'n':
                ReqNum=atoi(optarg);
                printf("Number of requests [%d]\n",ReqNum);
                break;
            case 'H':
                printf("Header Value [%s]\n",optarg);
                /* Set header options */
                if(myLib_append_header_req(newrq,optarg)!=MY_LIB_SUCCESS) {
                    printf("Failed to set header\n");
                }
                break;                
            default:
                printf("Option incorrect\n");
                return 1;
        }
    }

    /* Set the URL */
    if(myLib_set_url(newrq,UserURL)!=MY_LIB_SUCCESS) {
        printf("Failed to set url\n");
        myLib_del_req(newrq);
        myLib_cleanup();
        return EXIT_FAILURE;
    }
    
    /* Start the requests */
    for ( i=0; i < ReqNum; i++) {

        if(myLib_perform_req(newrq)!=MY_LIB_SUCCESS){
            failures++;
            continue;
        }
        
        info = myLib_get_info(newrq);
        if(!info) {
            failures++;
            continue;
        }
        
        /* Update stats */
        NETWORLLOOKUP_TIME +=info->namelookup_time;
        CONNECT_TIME +=info->connect_time;
        TRANSFER_TIME +=info->transfer_time;
        TOTAL_TIME += info->total_time;
        memcpy(&LASTIP_ADDRESS[0],info->ipaddr,strlen(info->ipaddr));
        LASTRESP_CODE = info->response_code;
        
        myLib_del_info(info);
    }

    /* This is mean, in case we need the statistical Median we should keep all 
    the data, sort them and get the (n/2)th dataset */
    
    validData= ReqNum - failures;
    if(validData){
        printf("SKTEST;%s;%ld;%f;%f;%f;%f\n",
               LASTIP_ADDRESS,LASTRESP_CODE,NETWORLLOOKUP_TIME/validData,
               CONNECT_TIME/validData,TRANSFER_TIME/validData,TOTAL_TIME/validData);
    }else{
        printf("All our tests failed! :(");
    }

    myLib_del_req(newrq);
    myLib_cleanup();
    
    return EXIT_SUCCESS;
}
