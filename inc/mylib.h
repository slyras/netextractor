#ifndef MYLIB_H_
#define MYLIB_H_

/* _MYLIB_STATUS_ */
#define MY_LIB_SUCCESS (1)
#define MY_LIB_FAILURE (-1)

#define MAXIMUM_IP_ADDRESS (40) /* IPV4-IPV6 */

/* Request object */
typedef struct{
    void* connection;
    void* headersList;
} myLib_req_t;

/* Response Stats */
typedef struct{
    char   ipaddr[MAXIMUM_IP_ADDRESS];  /* IP address of host     */
    long   response_code;               /* Responce Code          */
    double namelookup_time;             /* name lookup time in ms */
    double connect_time;                /* connection time in ms  */
    double transfer_time;               /* transfer time in ms    */
    double total_time;                  /* response time in ms    */
} myLib_info_t;

/* Initialize myLib
    IN: void
    OUT: _MYLIB_STATUS_
*/
int myLib_init();

/* Create new request object 
    IN: void
    OUT: Request object
*/
myLib_req_t* myLib_new_req();

/* Delete request object 
    IN: Request object
    OUT: _MYLIB_STATUS_
*/
int myLib_del_req(myLib_req_t* req);

/* Append header to request object 
    IN: Request object
    OUT: _MYLIB_STATUS_
*/
int myLib_append_header_req(myLib_req_t* req, const char* header);

/* Set fqdn on request object
    IN: Request object, url string
    OUT: _MYLIB_STATUS_
*/
int myLib_set_url(myLib_req_t* req, const char* url);

/* Perform the request
    IN: Request object
    OUT: _MYLIB_STATUS_
*/
int myLib_perform_req(myLib_req_t* req);

/* Get the information returned
    IN: Request object, url string
    OUT: Response Stats
*/
myLib_info_t* myLib_get_info(myLib_req_t* req);

/* Delete information object
    IN: Response Stats
    OUT: _MYLIB_STATUS_
*/
int myLib_del_info(myLib_info_t* info);

/* Cleanup any */
int myLib_cleanup();

#endif /* MYLIB_H_ */
