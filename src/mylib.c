#include "mylib.h"
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>


#ifdef NDEBUG 
#define MYLIB_DBG(m,...) \
            fprintf(stdout,"[%s][%d]"m"\r\n",__func__,__LINE__,##__VA_ARGS__);
#else
#define MYLIB_DBG(m,...)	;
#endif

/* Initilize my library */
int myLib_init(void) {
    
    if(curl_global_init(CURL_GLOBAL_DEFAULT)) {
        MYLIB_DBG("Init failed");
        return MY_LIB_FAILURE;
    }
    
    return MY_LIB_SUCCESS;
}

/* Create request object */
myLib_req_t* myLib_new_req() {
    
    CURL *curl = curl_easy_init();
    
    if(!curl){
        MYLIB_DBG("Core object init failed");
        return NULL;
    }
    
    myLib_req_t* req = malloc(sizeof(myLib_req_t));
    
    if(!req){
        MYLIB_DBG("Malloc failed");
        return NULL;
    }

    req->connection = (void*)curl;
    req->headersList = NULL;
        
    return req;
}

/* Delete request object */
int myLib_del_req(myLib_req_t* req) {
    
    if(!req){
        MYLIB_DBG("Invalid request object");
        return MY_LIB_FAILURE;
    }
    
    CURL *curl = (CURL*)req->connection;
    curl_easy_cleanup(curl);
    
    if(req->headersList){
        /* Clean any remaining headers */
        curl_slist_free_all((struct curl_slist *)req->headersList);
    }
    
    free(req);
    
    return MY_LIB_SUCCESS;    
}


/* Set url on request object */
int myLib_set_url(myLib_req_t* req, const char* url) {
    
    if(!req){
        MYLIB_DBG("Invalid request object");
        return MY_LIB_FAILURE;
    }
    
    CURL *curl = (CURL*)req->connection;
    
    if( (!strlen(url)) || (strncmp("http",url,strlen("http"))) ){
        MYLIB_DBG("invalid string");
        return MY_LIB_FAILURE;
    }
    
    if(!strncmp("https://",url,strlen("https://"))){
        /* Skip peer verification */
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
        /* Skip host verification */
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
    }

    if(curl_easy_setopt(curl, CURLOPT_URL,url)){
        MYLIB_DBG("Failed to set URL");
        return MY_LIB_FAILURE;
    }
    
    return MY_LIB_SUCCESS;
}

/* Append Headers in request */
int myLib_append_header_req(myLib_req_t* req, const char* header) {
    
    struct curl_slist *list = NULL;
    
    if( (!req) || (!strlen(header)) ){
        MYLIB_DBG("Invalid Parameters");
        return MY_LIB_FAILURE;
    }
    
    if(!req->headersList){
        MYLIB_DBG("No header, adding the first one!");
        req->headersList = (void*)curl_slist_append(NULL, header);
    }else{
        /* keep track of the old one in case of failure */
        list = (struct curl_slist *)req->headersList;
        MYLIB_DBG("Adding new header [%s]!",header);
        req->headersList = (void*)curl_slist_append(list, header);
        if(!req->headersList){
            MYLIB_DBG("Adding new header failed!");
            /* Reverting to old config */
            req->headersList = (void*) list;
            return MY_LIB_FAILURE;
        }
    }

    return MY_LIB_SUCCESS;
}


static size_t __my_lib_ignore_output( void *ptr, size_t size, size_t nmb, \
                                                                   void *stream)
{
    (void) ptr;
    (void) stream;
    MYLIB_DBG("Write output CB");
    return size * nmb;
}

/* Perform request */
int myLib_perform_req(myLib_req_t* req) {
    
    if(!req){
        MYLIB_DBG("Invalied req object");
        return MY_LIB_FAILURE;
    }
    
    CURL *curl = (CURL*)req->connection;
    
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &__my_lib_ignore_output);
    
    if(req->headersList){
        struct curl_slist *list = (struct curl_slist *)req->headersList;
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
    }
    
    CURLcode res = curl_easy_perform(curl);
    
    if(res != CURLE_OK){
        MYLIB_DBG("Request perform failed: %s\n", curl_easy_strerror(res));
        return MY_LIB_FAILURE;
    }
    
    MYLIB_DBG("Request Completed");
    
    return MY_LIB_SUCCESS;
}

/* Get the information returned */
myLib_info_t* myLib_get_info(myLib_req_t* req) {
    
    double namelookup_time=0.0;
    double connect_time=0.0;    
    double transfer_time=0.0;
    double total_time=0.0;
    long response_code=0;
    char *ipAddr = NULL;
    CURLcode res = CURLE_OK;
    
    if(!req){
        MYLIB_DBG("Invalid req object");
        return NULL;
    }
            
    myLib_info_t* info = malloc(sizeof(myLib_info_t));
    if(!info){
        MYLIB_DBG("Allocation Failed");
        return NULL;
    }
    
    memset(info,0,sizeof(myLib_info_t));
    /* Get all the information needed from specified request object */
    CURL *curl = (CURL*)req->connection;
    
    res = curl_easy_getinfo(curl,CURLINFO_NAMELOOKUP_TIME,&namelookup_time);
    
    res = curl_easy_getinfo(curl,CURLINFO_CONNECT_TIME,&connect_time);
       
    res = curl_easy_getinfo(curl,CURLINFO_STARTTRANSFER_TIME,&transfer_time);
    
    res = curl_easy_getinfo(curl,CURLINFO_TOTAL_TIME,&total_time);
    
    res = curl_easy_getinfo(curl,CURLINFO_PRIMARY_IP,&ipAddr);
    
    res = curl_easy_getinfo(curl,CURLINFO_RESPONSE_CODE,&response_code);
    
    if(res!=CURLE_OK){
        MYLIB_DBG("Get information failed %s",curl_easy_strerror(res));
        free(info);
        return NULL;
    }
    
    /* Pass the info to library information object */

    info->total_time = total_time;
    info->connect_time = connect_time;
    info->namelookup_time = namelookup_time;
    info->transfer_time = transfer_time;
    info->response_code = response_code;
    memcpy(info->ipaddr,ipAddr,strlen(ipAddr));
    
    return info;
}

/* Delete info object */
int myLib_del_info(myLib_info_t* info) {
    if(!info){
        MYLIB_DBG("Invalid info object");
        return MY_LIB_FAILURE;
    }
    free(info);
    return MY_LIB_SUCCESS;
}

/* Close and cleanup library */
int myLib_cleanup() {
    curl_global_cleanup();
    
    return MY_LIB_SUCCESS;
}
